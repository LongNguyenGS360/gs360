package com.qa.gs.automation.pageObjects.HomePage;


public class HomePageActions extends HomePageObjects{
    private static HomePageActions instance;
    private static HomePageObjects homePageObjects;

    public static HomePageActions getInstance() {
        if (instance == null) {
            instance = new HomePageActions();
            homePageObjects = new HomePageObjects();
        }
        return instance;
    }

    public void verifyEventsPageTitlePresent() {
        homePageObjects.verifyEventsPageTitlePresent();
    }

    public void clickOnCreateWebsiteButton() {
        homePageObjects.clickOnCreateWebsiteButton();
    }
}
