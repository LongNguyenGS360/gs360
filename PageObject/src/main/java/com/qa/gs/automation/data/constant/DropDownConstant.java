package com.qa.gs.automation.data.constant;

import lombok.Getter;
import lombok.Setter;

public class DropDownConstant {

    public enum MoreActionsMenu {
        EDIT_WEBSITE("Edit Website"),
        SET_WEBSITE_TO_OFFLINE("Set Website to Offline"),
        DELETE_WEBSITE("Delete Website");

        @Getter
        @Setter
        private String item;

        MoreActionsMenu(String item) {this.item = item;}
    }
}
