package com.qa.gs.automation.pageObjects.HomePage;

import com.qa.gs.automation.PageObjectBase;

public class HomePageObjects extends PageObjectBase {
    private static String eventsPageTitle = "//div[@class='dashboard']//div[@data-testid='dashboard-events-title']";
    private static String createWebsiteButton = "//button[contains(@class, MuiButton-root) and descendant::span[text" +
            "()" +
            "='Create Website']]";
    private static String actionMenu = "//button[@aria-label='more' and ancestor::tr[contains(@class, " +
            "'MuiTableRow-root')]/td[text()='%s']]";

    protected void verifyEventsPageTitlePresent() {
        seleniumWebDriverActions.isElementPresent(eventsPageTitle);
    }

    protected void clickOnCreateWebsiteButton() {
        seleniumWebDriverActions.clickControl(createWebsiteButton);
    }

    protected void selectActionMenuItem(String item) {
        seleniumWebDriverActions.clickControl(item);
    }

}
