package com.qa.gs.automation.pageObjects.LoginPage;

import com.qa.gs.automation.PageObjectBase;

public class LoginPageObjects extends PageObjectBase {
    private String loginEmailTextBox = "//input[@id='loginEmail']";
    private String loginPasswordTextBox = "//input[@id='loginPassword']";
    private String continueButton = "//button[@id='loginBtn']";
    private String loginButton = "//button[@data-testid='login-button']";


    protected void inputLoginEmail(String email) {
        seleniumWebDriverActions.inputText(loginEmailTextBox, email);
    }

    protected void inputLoginPassword(String password) {
        seleniumWebDriverActions.inputText(loginPasswordTextBox, password);
    }

    protected void clickContinueButton() {
        seleniumWebDriverActions.clickControl(continueButton);
    }

    protected void clickLoginButton() {
        seleniumWebDriverActions.clickControl(loginButton);
    }
}
