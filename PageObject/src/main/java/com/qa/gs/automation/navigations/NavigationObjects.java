package com.qa.gs.automation.navigations;

import com.qa.gs.automation.PageObjectBase;

public class NavigationObjects extends PageObjectBase {
    private static String siteBuilderItem = "//nav[@id='gs-navbar']//a[contains(@class, 'Site Builder')]";



    protected void gotoSiteBuilder() {
        seleniumWebDriverActions.clickControl(siteBuilderItem);
    }
}
