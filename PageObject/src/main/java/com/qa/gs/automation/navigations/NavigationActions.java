package com.qa.gs.automation.navigations;


import com.qa.gs.automation.PageObjectBase;

public class NavigationActions extends NavigationObjects {
    private static NavigationActions instance;
    private static NavigationObjects navigationObjects;

    public static NavigationActions getInstance() {
        if (instance == null) {
            instance = new NavigationActions();
            navigationObjects = new NavigationObjects();
        }
        return instance;
    }

    public void gotoSiteBuilderPage() {
        navigationObjects.gotoSiteBuilder();
    }
}
