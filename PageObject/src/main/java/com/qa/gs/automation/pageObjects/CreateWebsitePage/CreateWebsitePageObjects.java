package com.qa.gs.automation.pageObjects.CreateWebsitePage;

import com.qa.gs.automation.PageObjectBase;

public class CreateWebsitePageObjects extends PageObjectBase {
    private static String websiteTypeButton = "//div[contains(@class, 'MuiGrid-item') and descendant::span[text()" +
            "='%s']]";
    private static String brandDropdown = "//input[contains(@id, 'input') and ancestor::div/div[text()='Select " +
            "Brand']]";
    private static String propertiesDropdown = "//input[contains(@id, 'input') and ancestor::div/div[text()='Select " +
            "Property']]";
    private static String webSiteNameTextBox = "//input[@id='name']";
    private static String okButton = "//button[contains(@class, 'MuiButton-contained') and descendant::span[text()='OK']]";
    private static String templateButton = "//div[contains(@class, 'MuiGrid-container')]/div[1]";
    private static String saveAndEditLaterButton = "//button[contains(@class, 'MuiButton-contained') and " +
            "ancestor::div[@class='MuiCollapse-wrapperInner'] and descendant::span[text()='Save and Edit Later']]";
    private static String saveAndEditNowButton = "//button[contains(@class, 'MuiButton-contained') and " +
            "ancestor::div[@class='MuiCollapse-wrapperInner'] and descendant::span[text()='Save and Edit Now']]";



    protected void selectWebsiteType(String websiteType) {
        String wsTypeButton = String.format(websiteTypeButton, websiteType);
        seleniumWebDriverActions.clickControl(wsTypeButton);
    }

    protected void selectBrand(String brandName) {
        seleniumWebDriverActions.inputText(brandDropdown, brandName);
    }

    protected void selectProperty(String propertyName) {
        seleniumWebDriverActions.inputText(propertiesDropdown, propertyName);
    }

    protected void inputWebsiteName(String websiteName) {
        seleniumWebDriverActions.inputText(webSiteNameTextBox, websiteName);
    }

    protected void clickOKButton() {
        seleniumWebDriverActions.clickControl(okButton);
    }

    protected void selectTemplateButton() {
        //String templateWebsite = String.format(templateButton, templateName);
        seleniumWebDriverActions.clickControl(templateButton);
    }

    protected void clickOnSaveAndEditLaterButton() {
        seleniumWebDriverActions.clickControl(saveAndEditLaterButton);
    }

    protected void clickOnSaveAndEditNowButton() {
        seleniumWebDriverActions.clickControl(saveAndEditNowButton);
    }



}
