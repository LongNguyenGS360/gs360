package com.qa.gs.automation.data.constant;

import lombok.Getter;
import lombok.Setter;

public class WebsiteConstant {

    public enum WebsiteType {
        HOTEL_PRIVATE_BOOKING_SITE("Hotel private label booking site"),
        GUEST_RESERVATION_BOOKING_SITE("Guest reservation booking site"),
        E_COMMERCE_SITE("E-Commerce Site");

        @Getter
        @Setter
        private String websiteType;

        WebsiteType(String websiteType) {this.websiteType = websiteType;}
    }


}
