package com.qa.gs.automation;

import Common.Random;
import com.qa.gs.automation.SeleniumWebDriver.SeleniumWebDriverWrapper;

public class PageObjectBase {
    public SeleniumWebDriverWrapper seleniumWebDriverActions;
    public Random random;

    public PageObjectBase() {
        seleniumWebDriverActions = SeleniumWebDriverWrapper.getInstance();
    }
}
