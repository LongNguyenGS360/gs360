package com.qa.gs.automation.data.objects;

import Common.Random;
import com.qa.gs.automation.data.constant.WebsiteConstant;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class WebsiteInfo {
    private String websiteType;
    private String brandName;
    private String propertyName;
    private String websiteName;
    private String templateName;

    public WebsiteInfo() {
        Random random = new Random();
        this.websiteType = WebsiteConstant.WebsiteType.HOTEL_PRIVATE_BOOKING_SITE.getWebsiteType();
        this.brandName = "JW Marriott";
        this.propertyName = "JW Marriott";
        this.websiteName = random.getAlphaString() + " Website";
    }
}
