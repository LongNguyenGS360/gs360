package com.qa.gs.automation.pageObjects.CreateWebsitePage;

import com.qa.gs.automation.data.objects.WebsiteInfo;

public class CreateWebsitePageActions {
    private static CreateWebsitePageActions instance;
    private static CreateWebsitePageObjects createWebsitePageObjects;

    public static CreateWebsitePageActions getInstance() {
        if (instance == null) {
            instance = new CreateWebsitePageActions();
            createWebsitePageObjects = new CreateWebsitePageObjects();
        }
        return instance;
    }

    public void selectWebsiteType(String websiteType) {
        createWebsitePageObjects.selectWebsiteType(websiteType);
    }

    public void selectBrand(String brandName) {
        createWebsitePageObjects.selectBrand(brandName);
    }

    public void selectProperty(String propertyName) {
        createWebsitePageObjects.selectProperty(propertyName);
    }

    public void inputWebsiteName(String websiteName) {
        createWebsitePageObjects.inputWebsiteName(websiteName);
    }

    public void clickOKButton() {
        createWebsitePageObjects.clickOKButton();
    }

    public void selectTemplate() {
        createWebsitePageObjects.selectTemplateButton();
    }

    public void clickOnSaveAndEditLaterButton() {
        createWebsitePageObjects.clickOnSaveAndEditLaterButton();
    }

    public void clickOnSaveAndEditNowButton() {
        createWebsitePageObjects.clickOnSaveAndEditNowButton();
    }

    public void createWebSiteWithoutEdit(WebsiteInfo websiteInfo) {
        selectWebsiteType(websiteInfo.getWebsiteType());
        selectBrand(websiteInfo.getBrandName());
        selectProperty(websiteInfo.getPropertyName());
        inputWebsiteName(websiteInfo.getWebsiteName());
        selectTemplate();
        clickOnSaveAndEditLaterButton();
    }

    public void createWebSiteWithEditing(WebsiteInfo websiteInfo) {
        selectWebsiteType(websiteInfo.getWebsiteType());
        selectBrand(websiteInfo.getBrandName());
        selectProperty(websiteInfo.getPropertyName());
        inputWebsiteName(websiteInfo.getWebsiteName());
        selectTemplate();
        clickOnSaveAndEditNowButton();
    }




}
