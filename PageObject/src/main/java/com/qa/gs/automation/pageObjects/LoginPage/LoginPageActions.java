package com.qa.gs.automation.pageObjects.LoginPage;

public class LoginPageActions extends LoginPageObjects{
    private static LoginPageActions instance;
    private static LoginPageObjects loginPageObjects;

    public static LoginPageActions getInstance() {
        if (instance == null) {
            instance = new LoginPageActions();
            loginPageObjects = new LoginPageObjects();
        }
        return instance;
    }

    public void inputEmailUserName(String email) {
        loginPageObjects.inputLoginEmail(email);
    }

    public void inputPassword(String password) {
        loginPageObjects.inputLoginPassword(password);
    }

    public void clickContinueButton() {
        loginPageObjects.clickContinueButton();
    }

    public void clickLoginButton() {
        loginPageObjects.clickLoginButton();
    }

    public void login(String email, String pass) {
        inputEmailUserName(email);
        clickContinueButton();
        inputPassword(pass);
        clickLoginButton();
    }
}
