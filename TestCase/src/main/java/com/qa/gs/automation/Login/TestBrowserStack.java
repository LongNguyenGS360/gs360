package com.qa.gs.automation.Login;

import com.qa.gs.automation.TestCaseBase;
import org.testng.annotations.Test;

public class TestBrowserStack extends TestCaseBase {

    @Test
    public void gotoGroupSyncPlatform() {
        browseUrl("https://dev.groupsync.com/login");
    }

    @Test(dependsOnMethods = {"gotoGroupSyncPlatform"})
    public void login() {
        uiTester.loginPageActions.inputEmailUserName("admin@example.com");
        uiTester.loginPageActions.clickContinueButton();
        uiTester.loginPageActions.inputPassword("password");
        uiTester.loginPageActions.clickLoginButton();
    }

    @Test(dependsOnMethods = {"login"})
    public void verifyLoginSuccessfully() {
        uiTester.homePageActions.verifyEventsPageTitlePresent();
    }
}
