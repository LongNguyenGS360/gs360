package com.qa.gs.automation;

import com.qa.gs.automation.navigations.NavigationActions;
import com.qa.gs.automation.pageObjects.CreateWebsitePage.CreateWebsitePageActions;
import com.qa.gs.automation.pageObjects.HomePage.HomePageActions;
import com.qa.gs.automation.pageObjects.LoginPage.LoginPageActions;

public class UiTester {
    public static UiTester uiTester;

    public NavigationActions navigationActions = NavigationActions.getInstance();
    public LoginPageActions loginPageActions = LoginPageActions.getInstance();
    public HomePageActions homePageActions = HomePageActions.getInstance();
    public CreateWebsitePageActions createWebsitePageActions = CreateWebsitePageActions.getInstance();
}
