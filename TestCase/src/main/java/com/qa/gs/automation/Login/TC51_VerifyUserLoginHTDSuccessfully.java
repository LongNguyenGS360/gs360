package com.qa.gs.automation.Login;

import com.qa.gs.automation.TestCaseBase;
import org.testng.annotations.AfterClass;
import org.testng.annotations.Test;

public class TC51_VerifyUserLoginHTDSuccessfully extends TestCaseBase {

    @Test
    public void gotoGroupSyncPlatForm() {
        browseUrl("https://dev.groupsync.com/login");
    }

    @Test(dependsOnMethods = {"gotoGroupSyncPlatForm"})
    public void login() {
        uiTester.loginPageActions.login("admin@example.com", "password");

//        uiTester.loginPageActions.inputEmailUserName("admin@example.com");
//        uiTester.loginPageActions.clickContinueButton();
//        uiTester.loginPageActions.inputPassword("password");
//        uiTester.loginPageActions.clickLoginButton();
    }

    @Test(dependsOnMethods = {"login"})
    public void verifyLoginSuccessfully() {
        uiTester.homePageActions.verifyEventsPageTitlePresent();
    }
}
