package com.qa.gs.automation;

import SeleniumFactory.DriverManagerFactory;
import com.qa.gs.automation.SeleniumWebDriver.SeleniumWebDriverWrapper;
import io.github.bonigarcia.wdm.WebDriverManager;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;

import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.util.Properties;
import java.util.concurrent.TimeUnit;
import java.net.URL;

public class TestCaseBase extends UiTester {
    public static final String USERNAME = "paulsmith79";
    public static final String AUTOMATE_KEY = "yWorworfrqJic7yME2E6";
    private static final Logger logger = LogManager.getLogger(TestCaseBase.class);
    public static final String URL_BROWSER_STACK = "https://" + USERNAME + ":" + AUTOMATE_KEY + "@hub-cloud.browserstack.com/wd/hub";
    public WebDriver webDriver;
    public DriverManagerFactory driverManagerFactory = new DriverManagerFactory();


    public void init() throws IOException {

        WebDriverManager.chromedriver().setup();
        //String headless = getPropertyFromResourcesFile("headlessMode");

        webDriver = driverManagerFactory.getDriverManger("CHROME");
        SeleniumWebDriverWrapper seleniumWebDriverWrapper = SeleniumWebDriverWrapper.getInstance();
        seleniumWebDriverWrapper.setWebDriver(webDriver);
        uiTester = new UiTester();
    }

    @BeforeClass
    public void runOnBrowserStack() throws MalformedURLException {
        DesiredCapabilities desiredCapabilities = new DesiredCapabilities();

        desiredCapabilities.setCapability("browserstack.idleTimeout", "900");
        desiredCapabilities.setCapability("os", "Windows");
        desiredCapabilities.setCapability("os_version", "10");
        desiredCapabilities.setCapability("browser", "Chrome");
        desiredCapabilities.setCapability("browser_version", "89");
        desiredCapabilities.setCapability("name", "My First Test");
        desiredCapabilities.setCapability("build", "Build #1");
        desiredCapabilities.setCapability("project", "Sample project");

        WebDriverManager.chromedriver().setup();
        webDriver = new RemoteWebDriver(new URL(URL_BROWSER_STACK), desiredCapabilities);

        SeleniumWebDriverWrapper seleniumWebDriverWrapper = SeleniumWebDriverWrapper.getInstance();
        seleniumWebDriverWrapper.setWebDriver(webDriver);
        uiTester = new UiTester();
    }

    @AfterClass
    public void tearDown() {
        webDriver.close();
        webDriver.quit();
    }

    public void browseUrl(String url) {
        logger.info("Launch Browser - %s", url);
        webDriver.get(url);
        webDriver.manage().window().maximize();
        webDriver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
    }

    public String getPropertyFromResourcesFile(String propertyName) throws IOException {
        Properties properties = new Properties();
        InputStream is = TestCaseBase.class.getResourceAsStream("/my-properties.properties");
        properties.load(is);

        return properties.getProperty(propertyName);
    }
}
