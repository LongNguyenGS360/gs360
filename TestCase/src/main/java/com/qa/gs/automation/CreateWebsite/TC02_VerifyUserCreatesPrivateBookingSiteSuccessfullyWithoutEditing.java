package com.qa.gs.automation.CreateWebsite;

import com.qa.gs.automation.TestCaseBase;
import com.qa.gs.automation.data.constant.WebsiteConstant;
import com.qa.gs.automation.data.objects.WebsiteInfo;
import org.apache.commons.exec.DaemonExecutor;
import org.testng.annotations.Test;

public class TC02_VerifyUserCreatesPrivateBookingSiteSuccessfullyWithoutEditing extends TestCaseBase {
    private WebsiteInfo websiteInfo = new WebsiteInfo();
    private final static String BRAND_NAME = "JW Marriott";
    private final static String PROPERTY_NAME = "JW Marriott";

    @Test
    public void gotoGroupSyncPlatForm() {
        browseUrl("https://dev.groupsync.com/login");
    }

    @Test(dependsOnMethods = {"gotoGroupSyncPlatForm"})
    public void login() {
        loginPageActions.login("admin@example.com", "password");
    }

    @Test(dependsOnMethods = {"login"})
    public void gotoSiteBuilderPage() {
        uiTester.navigationActions.gotoSiteBuilderPage();
    }

    @Test(dependsOnMethods = ("gotoSiteBuilderPage"))
    public void clickOnCreateNewButton() {
        uiTester.homePageActions.clickOnCreateWebsiteButton();
    }

    @Test(dependsOnMethods = {"clickOnCreateNewButton"})
    public void selectHotelPrivateLabelBookingSiteButton() {
        uiTester.createWebsitePageActions.selectWebsiteType(WebsiteConstant.WebsiteType.HOTEL_PRIVATE_BOOKING_SITE.getWebsiteType());
    }

    @Test(dependsOnMethods = {"selectHotelPrivateLabelBookingSiteButton"})
    public void selectBrandOption() {
        uiTester.createWebsitePageActions.selectBrand(websiteInfo.getBrandName());
    }

    @Test(dependsOnMethods = {"selectBrandOption"})
    public void selectPropertiesOption() {
        uiTester.createWebsitePageActions.selectProperty(websiteInfo.getPropertyName());
    }

    @Test(dependsOnMethods = {"selectPropertiesOption"})
    public void fillWebsiteName() {
        uiTester.createWebsitePageActions.inputWebsiteName(websiteInfo.getWebsiteName());
    }

    @Test(dependsOnMethods = {"fillWebsiteName"})
    public void clickOKButton() {
        uiTester.createWebsitePageActions.clickOKButton();
    }

    @Test(dependsOnMethods = {"clickOKButton"})
    public void selectTemplate() {
        uiTester.createWebsitePageActions.selectTemplate();
    }

    @Test(dependsOnMethods = {"selectTemplate"})
    public void clickSaveAndEditLaterButton() {
        uiTester.createWebsitePageActions.clickOnSaveAndEditLaterButton();
    }

    @Test(dependsOnMethods = {"clickSaveAndEditLaterButton"})
    public void setWebsitePublic() {
        
    }

    @Test(dependsOnMethods = {"setWebsitePublic"})
    public void launchWebsite() {

    }
}
