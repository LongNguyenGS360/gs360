package SeleniumFactory;

import org.openqa.selenium.WebDriver;

public class DriverManagerFactory {

    public WebDriver getDriverManger(String browserType) {
        WebDriver webDriver = null;
        switch (browserType) {
            case "CHROME":
                ChromeDriverManager chromeDriverManager = new ChromeDriverManager();
                webDriver = chromeDriverManager.createWebDriver();
                break;
            case "FIREFOX":
                System.out.println("Define FireFoxDriverManager");
                break;
            default:
                System.out.println("Building");
                break;
        }
        return webDriver;
    }
}
