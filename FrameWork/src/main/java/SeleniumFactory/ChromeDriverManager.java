package SeleniumFactory;

import io.github.bonigarcia.wdm.WebDriverManager;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;

public class ChromeDriverManager {
    private WebDriver driver;
    public WebDriver createWebDriver() {
        WebDriverManager.chromedriver().setup();
        ChromeOptions options = setupChromeOptions("false");
        driver = new ChromeDriver(options);
        return driver;
    }

    private ChromeOptions setupChromeOptions(String headlessProperty) {

        ChromeOptions chromeOptions = new ChromeOptions();
        if (headlessProperty == "true") {
            chromeOptions.addArguments("--headless");
            System.out.println("Headless mode enabled");
        } else {
            System.out.println("Headless mode disabled");
        }
        chromeOptions.addArguments("--no-sandbox");
        chromeOptions.addArguments("--disable-dev-shm-usage");

        return chromeOptions;
    }


}
