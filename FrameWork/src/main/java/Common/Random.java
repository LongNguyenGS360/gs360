package Common;

public class Random {
    private String originString = "ABCDEFGHIJKLMNOPQRSTUWXYZabcdefghijklmnopqrstuwxyz";
    public String getAlphaString() {
        StringBuilder sb = new StringBuilder();

        for (int i=0; i < 10; i++) {
            int index = (int) (originString.length() + Math.random());
            sb.append(originString.charAt(index));
        }

        return sb.toString();
    }
}
