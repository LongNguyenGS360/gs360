package com.qa.gs.automation.SeleniumWebDriver;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.*;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;

public class SeleniumWebDriverWrapper implements SeleniumWebDriverInterface{
    private WebDriver driver;
    private JavascriptExecutor javascriptExecutor;
    private static SeleniumWebDriverWrapper instance;
    private static final Logger logger = LogManager.getLogger(SeleniumWebDriverWrapper.class);

    public static SeleniumWebDriverWrapper getInstance() {
        if (instance == null) {
            instance = new SeleniumWebDriverWrapper();
        }
        return instance;
    }

    public void setWebDriver(WebDriver webDriver) {
        if (driver == null) {
            driver = webDriver;
            javascriptExecutor = (JavascriptExecutor) driver;
        }
    }

    @Override
    public void inputText(String control, String value) {
        logger.info("Input Text - %s", value);
        WebElement webElement = getElement(control);
        webElement.sendKeys(value);
    }

    @Override
    public void clickControl(String control) {
        logger.info("Click Control - %s", control);
        WebElement webElement = getElement(control);
        webElement.click();
    }

    @Override
    public void isElementPresent(String control) {
        logger.info("Is WebElement Present - %s", control);
        WebElement webElement = getElement(control, 30);
        javascriptExecutor = (JavascriptExecutor) driver;
        if (webElement == null) {
            Assert.assertTrue(false);
            javascriptExecutor.executeScript("browserstack_executor: {\"action\": \"setSessionStatus\", \"arguments\": {\"status\": " +
                    "\"failed\", \"reason\": \"Title not matched!\"}}");
        } else {
            Assert.assertTrue(true);
            javascriptExecutor.executeScript("browserstack_executor: {\"action\": \"setSessionStatus\", \"arguments\": {\"status\": " +
                    "\"passed\", \"reason\": \"Title matched!\"}}");
        }
    }

    public void getPropertyInXMLFile() {
        
    }

    private WebElement getElement (String xPath,int timeOut){
        WebDriverWait webDriverWait = new WebDriverWait(driver, timeOut);
        webDriverWait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(xPath)));
        WebElement webElement = driver.findElement(By.xpath(xPath));

        return webElement;
    }

    private WebElement getElement(String xPath) {
        WebDriverWait webDriverWait = new WebDriverWait(driver, 10);
        webDriverWait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(xPath)));
        WebElement webElement = driver.findElement(By.xpath(xPath));

        return webElement;
    }
}
