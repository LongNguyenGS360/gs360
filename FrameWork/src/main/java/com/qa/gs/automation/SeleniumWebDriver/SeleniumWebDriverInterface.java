package com.qa.gs.automation.SeleniumWebDriver;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.WebDriverWait;

public interface SeleniumWebDriverInterface {

    public void inputText(String control, String value);

    public void clickControl (String control);

    public void isElementPresent(String control);
}
