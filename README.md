# README #

### What is this repository for? ###

* This is the automation project that is build on top of SeleniumWebDriver using Java language. QA Team can use this framework to implement the automation test cases, execute test cases and get the results.
* Version: 1.0.0

## INTRODUCTION ##

###Tools & framework
    * Maven Project
    * TestNG Report
    * POM model
    * SeleniumWebDriver

### Structure ####
* FrameWork Module
    * Contain the high level functions that are combined from basic function that is provided by Selenium framework.
    * Configure and manage multiple Drivers to run script on multiple Browser.
    * Has the separate pom.xml file to contain dependencies that are only used in Framework Module.
    * Dependency on parent Module to use the common dependency such as log4j.
    * Being improved to be able to work with any Project.
* PageObject Module
    * Contains PageObjects and PageObjectActions classes.
    * PageObjects is used to capture webElement (using xPath) and define the basic functions on that page only.
    * PageObjectActions is used to define the basic functions that are called from PageObjects and high level functions
        that are combination from basic functions.
    * The functions are able to be a step or validation.
* TestCase Module
    * Define all test cases of the project
    * The structures should be based on TestLinks structure (http://54.169.182.61/testlink/login.php - admin/admin)
    * Call directly functions from PageObjectActions class only.
### Dependencies ###
* SeleniumWebDriver
* TestNG
* Log4J
### How to run tests ###
    1. Open TestCases Module -> src/main/java/com.qa.gs.automation/
    2. Run single test cases from IDE directly

### Being improved ###
* Run test cases via test plan from commandline.
* Deploy Jenkins to schedule running test cases.
* Run multiple browser that is set from configuration file.
* Add more module for API Testing automation test.
* Synchronize test report with TestLinks to update report status to TestLinks automatically.